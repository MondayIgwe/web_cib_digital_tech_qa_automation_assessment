package Web_Test;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;


public class Execution extends SetParameters {

    private final String URL = "http://www.way2automation.com/angularjs-protractor/webtables/";
    private WebDriver driver = null;


    /* REQUIREMENTS
    * Navigate to way to automation webtable
    * Validate that you are on the User List Table
    * Click Add user
    * Add users with the following details
    * Ensure that User Name (*) is unique on each run
    * Ensure that your users are added to the list
     */


    @org.testng.annotations.Test
    public void web_Task_2() throws Exception {
        BaseClass.setPath();
        driver = new ChromeDriver();
        driver.get(URL);
        driver.manage().window().maximize();
        String print = driver.getTitle();
        System.out.print(print);

        addUsers_FirstUser("Johnny","Johnson","johnny","@admin","Admin","admin@mail.com","0825554657");
        Thread.sleep(2000);

        addUsers_SecondUser("Kingsley","Chucks","kingChu","@customer","Customer","customer@mail.com","0834449856");
        Thread.sleep(2000);

        tearDown_Execution();
    }

}