REQUIREMENTS
===================
1, Navigate to way to automation webtable
2, Validate that you are on the User List Table
3, Click Add user
4, Add users with the following details
5, Ensure that User Name (*) is unique on each run
6, Ensure that your users are added to the list


TOOLS USED
==========
Intellij IDE
Maven Framework
Java & Selenium Libraries
TestNG Libraries



TO VIEW REPORT
=================
Run the testng.xml file
After execution, click the test-output folder
Right-click nd open the .html file in any browser to view the executed reports




Found a Defect with the Application Under Test
=======================================
Description: Every time the user submits the form to the server, the Company radio buttons automatically disables
             which makes the company column on the User List Table empty after executing the scenario.

             Alternatively, The Uer List Table is Locked, So a new user cannot be successfully added to the table.


Reproduced the bug manually by executing the same test scenario and it appears it a technical Defect from the developers side.


Defect should be kept open at the moment until fixed....
confirmation and regression testing must be performed after fixes are made.

